# Mixly多合一空气质量传感器（RS485）图形化库

#### 介绍

本图形化库是基于 RS485型多合一空气质量传感器 采集上报的信息进行解析处理，做成直接可以供大家拖拽使用该传感器上报的参数数据，如  **温度**  、  **湿度**   、  **CO2**  、  **TVOC**  、  **CH2O(甲醛)**  、  **PM10**  、  **PM2.5**  。

##### 注意：本例程所用到的库文件在 https://gitee.com/LNSFAIoT/Mixly-LNSFAIoT-custom-library 下，其仓库名为  **Mixly 岭师人工智能素养教育共同体-自定义库** 。

 **测试硬件** ：

https://shop221040643.taobao.com

多合一空气质量传感器 图形化编程 自研 提供源码 中小学 创客 AI

https://item.taobao.com/item.htm?id=678612499027

### 岭师人工智能素养教育共同体PBL项目教程系列

 **岭师人工智能素养教育共同体PBL项目教程系列之1：户外智慧农场项目实战系列** 

【PBL项目实战】户外智慧农场项目实战系列之1——阿里云物联网平台的开通与云端可视化应用的新建

https://mp.weixin.qq.com/s/avKWJHNvnPeFsdEXUBByjw

【PBL项目实战】户外智慧农场项目实战系列之2——产品与设备的新建及与云端可视化应用的关联

https://mp.weixin.qq.com/s/mH4bRSf93QC4Jhhd5ci2mQ

【PBL项目实战】户外智慧农场项目实战系列之3——云端应用可视化页面开发及设备数据源的配置与调试

https://mp.weixin.qq.com/s/5OT57-_QlN7lwRMWvey4lw

【PBL项目实战】户外智慧农场项目实战系列之4——Mind+Mixly双平台ESP32数据上云及云端可视化实时展示

https://mp.weixin.qq.com/s/r_NeJdPoio9IVselx6Ru1Q

【PBL项目实战】户外智慧农场项目实战系列之5——天气预报API接口对接

https://mp.weixin.qq.com/s/jditkIEd-UK6cSQyCC_-Eg

【PBL项目实战】户外智慧农场项目实战系列之6——文本与图片、背景云端响应

https://mp.weixin.qq.com/s/eIozKfmCBo8Afp5QoorqWQ

 **【PBL项目实战】户外智慧农场项目实战系列之7——Mind+Mixly双平台RS485工业级多合一空气质量传感器数据上云及云端可视化实时展示** 

  **https://mp.weixin.qq.com/s/ZEUEEo91pBoxjRlhjGzrjA** 

【PBL项目实战】户外智慧农场项目实战系列之8——Mind+Mixly双平台RS485工业级多合一土质检测传感器数据上云及云端可视化实时展示

【PBL项目实战】户外智慧农场项目实战系列之9——Mind+Mixly双平台RS485工业级多合一气象传感器数据上云及云端可视化实时展示

【PBL项目实战】户外智慧农场项目实战系列之10——植物园花果识别与云端实时同步

【PBL项目实战】户外智慧农场项目实战系列之11——云端视频流直播

【PBL项目实战】户外智慧农场项目实战系列之12——图表、二维数据表等可视化应用


【PBL项目实战】户外智慧农场项目实战系列之13——Mind+Mixly双平台LED屏本地大屏显示实现

### 软件架构

基于Mixly2.0 RC3开发。

### 安装教程

利用【 https://gitee.com/LNSFAIoT/Mixly-LNSFAIoT-custom-library.git 】克隆到本地文件夹，打开 mixly.exe 官方软件导入库文件，库文件的后缀名为【 .xml 】，利用本开源项目配套提供的测试代码进行测试。

### 使用说明

#### 积木截图

![输入图片说明](image/Mixly%20%E5%A4%9A%E5%90%88%E4%B8%80%E7%A9%BA%E6%B0%94%E8%B4%A8%E9%87%8F%E4%BC%A0%E6%84%9F%E5%99%A8%E7%9A%84%E7%A7%AF%E6%9C%A8%E6%88%AA%E5%9B%BE.png)

#### RS485型-多合一传感器测试代码的截图

![输入图片说明](image/Mixly%20ESP32E-%E5%A4%9A%E5%90%88%E4%B8%80%E7%A9%BA%E6%B0%94%E8%B4%A8%E9%87%8F%E4%BC%A0%E6%84%9F%E5%99%A8%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81.png)

#### RS485型-上报阿里云IoT-多合一传感器测试代码的截图

![输入图片说明](image/Mixly%20ESP32E-%E5%A4%9A%E5%90%88%E4%B8%80%E7%A9%BA%E6%B0%94%E8%B4%A8%E9%87%8F%E4%BC%A0%E6%84%9F%E5%99%A8%E9%98%BF%E9%87%8C%E4%BA%91%E4%B8%8A%E4%BA%91%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81.png)

### 参与贡献

 **岭师人工智能素养教育共同体** 

### 如何加入

 **请关注微信公众号：人工智能素养教育共同体。** 

![输入图片说明](image/%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E7%B4%A0%E5%85%BB%E6%95%99%E8%82%B2%E5%85%B1%E5%90%8C%E4%BD%93%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)

### 捐助

如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。

![输入图片说明](image/%E8%AF%B7%E5%A4%9A%E5%A4%9A%E6%94%AF%E6%8C%81%E6%88%91%E4%BB%AC.jpg)

### 联系我们

有关 **意见反馈** 和 **合作意向** 联系，可扫码添加以下 **企业微信** 。

#### 黄老师：

![输入图片说明](image/%E9%BB%84%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)

#### 曾老师：

![输入图片说明](image/%E6%9B%BE%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)